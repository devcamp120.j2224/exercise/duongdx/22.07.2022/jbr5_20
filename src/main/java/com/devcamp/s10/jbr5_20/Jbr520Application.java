package com.devcamp.s10.jbr5_20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr520Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr520Application.class, args);
	}

}
