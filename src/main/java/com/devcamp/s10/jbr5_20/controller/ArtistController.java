package com.devcamp.s10.jbr5_20.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s10.jbr5_20.model.Artist;
import com.devcamp.s10.jbr5_20.service.ArtistService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ArtistController {
    @Autowired
    private ArtistService artistService;
    @GetMapping("/artists")
    public ArrayList<Artist> getArtistList(){
        ArrayList<Artist> artistList = artistService.getArtist();
        return artistList;
    }

    @GetMapping("/artist-info")
    public Artist getArtistInfo(@RequestParam(required = true, name="ArtistCode") Integer artistCode){

        ArrayList<Artist> artistList = artistService.getArtist();

            Artist artistFind = new Artist();

        for(Artist artist : artistList){
           if(artist.getId() == artistCode){
                artistFind = artist;
                }
            }
        return artistFind;
    }
}
