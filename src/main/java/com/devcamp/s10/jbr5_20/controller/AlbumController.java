package com.devcamp.s10.jbr5_20.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s10.jbr5_20.model.Album;
import com.devcamp.s10.jbr5_20.service.AlbumService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class AlbumController {
    @Autowired
    private AlbumService albumService;
    @GetMapping("/album-info")
    public Album getInfoAlbum(@RequestParam("id") Integer id) {
        ArrayList<Album> albums = albumService.getAlbumAll();

        Album albumfind = new Album();
        for (Album album : albums) {  
            if(album.getId() == id) {
                albumfind = album ;
            }
        }
        return albumfind ;
    }

}
