package com.devcamp.s10.jbr5_20.model;

import java.util.ArrayList;

public class Artist {
    private int id;
    private String name;
    private ArrayList<Album> Album ;

    public Artist() {
    }

    public Artist(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public Artist(int id, String name, ArrayList<com.devcamp.s10.jbr5_20.model.Album> album) {
        this.id = id;
        this.name = name;
        Album = album;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Album> getAlbum() {
        return Album;
    }

    public void setAlbum(ArrayList<Album> album) {
        Album = album;
    }
  
}
