package com.devcamp.s10.jbr5_20.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.devcamp.s10.jbr5_20.model.Artist;

@Service
public class ArtistService {
    @Autowired
    private AlbumService albumService;
    Artist xuanDuong = new Artist(1, "Duong");
    Artist ThuyLinh = new Artist(2, "ThuyLinh");
    Artist Linh = new Artist(3, "Linh");

    public ArrayList<Artist> getArtist() {
        xuanDuong.setAlbum(albumService.getAlbum1());
        ThuyLinh.setAlbum(albumService.getAlbum2());
        Linh.setAlbum(albumService. getAlbum3());

        ArrayList<Artist> alListAll = new ArrayList<Artist>();
        alListAll.add(xuanDuong);
        alListAll.add(ThuyLinh);
        alListAll.add(Linh);

        return alListAll;
    }
}
