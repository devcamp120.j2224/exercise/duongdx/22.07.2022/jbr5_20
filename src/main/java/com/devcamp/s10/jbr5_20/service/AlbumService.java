package com.devcamp.s10.jbr5_20.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s10.jbr5_20.model.Album;

@Service
public class AlbumService {
    Album ThienNhien = new Album(2, "ThienNhien");
    Album CayCoi = new Album(3, "CayCoi");
    Album RungNui = new Album(7, "RungNui");
    Album HocSinh = new Album(4, "HocSinh");
    Album GiaoVien = new Album(5, "GiaoVien");
    Album TruongHoc = new Album(1, "TruongHoc");
    Album Que = new Album(6, "Que");
    Album DongVat = new Album(8, "DongVat");
    Album Tinhyeu = new Album(9, "Tinhyeu");
   
    public ArrayList<Album> getAlbum1() {
        ArrayList<Album> alList1 = new  ArrayList<Album>();
        alList1.add(ThienNhien);
        alList1.add(CayCoi);
        alList1.add(RungNui);

        return alList1 ;
    }

    public ArrayList<Album> getAlbum2() {
        ArrayList<Album> alList2 = new  ArrayList<Album>();
        alList2.add(HocSinh);
        alList2.add(GiaoVien);
        alList2.add(TruongHoc);

        return alList2 ;
    }

    public ArrayList<Album> getAlbum3() {
        ArrayList<Album> alList3 = new  ArrayList<Album>();
        alList3.add(Que);
        alList3.add(DongVat);
        alList3.add(Tinhyeu);

        return alList3 ;
    }

    public ArrayList<Album> getAlbumAll() {
        ArrayList<Album> alList = new  ArrayList<Album>();
        alList.add(ThienNhien);
        alList.add(CayCoi);
        alList.add(RungNui);
        alList.add(HocSinh);
        alList.add(GiaoVien);
        alList.add(TruongHoc);
        alList.add(Que);
        alList.add(DongVat);
        alList.add(Tinhyeu);

        return alList ;
    }
}
